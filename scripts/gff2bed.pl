#!/usr/bin/perl

my $in = shift;
my $prefix = shift;
my $out = shift;
open(IN,$in);
open(OUT,">$out");
while (my $line = <IN>) {
    chomp $line;
    next if $line =~ /^#/;  # Skip comment lines
    my @fields = split("\t", $line);
    if (@fields < 9) {
        warn "Skipping line with less than 9 fields: $line\n";
        next;
    }
    
    my ($seqname, $source, $type, $start, $end, $score, $strand, $frame, $attributes) = @fields;
    if ($type eq "gene") {
        my $bed_start = $start - 1;  # BED is 0-based
        my $bed_end = $end;
        my $bed_name = '';
    
        # Parse attributes to get the name
        if ($attributes =~ /Name=([^;]+)/) {
            $bed_name = $prefix ."_". $1;
        }
        # Print bed file
        print OUT join("\t", $seqname, $bed_start, $bed_end, $bed_name),"\n";
    }
}
    
close IN;
close OUT;