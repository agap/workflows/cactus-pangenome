
use File::Basename;
use Graph::Undirected; 
use Getopt::Long;
use Pod::Usage;
use FindBin;
my $graph = Graph::Undirected->new; 

my $bed         = "";
my $comparison  = "";
my $unmapped    = "";
my $directory   = "";
my $help        = "";
my $usage       = $FindBin::Bin ."/". $FindBin::Script.q/ --help

Parameters
       --bed    bed file for gene [required]
       --comparaison  bed comparison file
       --unmapped     Unmapped gene provide by liftoff
       --directory    Directory for output
       --seqfile      Seqfile
       --help    show this help and exit
/;
GetOptions(
     'bed=s{1,}'  => \@bed,
     'comparison=s{1,}' => \@comparison,
     'unmapped=s{1,}' => \@unmapped,
     'directory=s'     => \$directory,
     'help|h|?'  => \$help
)  or pod2usage(-message => $usage);
if ($help) { pod2usage(-message => $usage); }



my $temp_pav       = $directory ."/temp_pav.txt";
my $keep_id        = $directory ."/keep_id_unsorted.txt";
my $keep_id_sorted = $directory ."/keep.txt";
my $upset          = $directory ."/pav.csv";
my $pav            = $directory ."/pav_unsorted.txt";
my $stat            = $directory ."/pav.stat";
my $pav_sorted     = $directory ."/pav.txt"; 
my @species;
my @init_rvalue;
my @init_value;
foreach my $bed (@bed) {
    my ($name,$path) = fileparse($bed,".bed");
    push @species, $name;
    push @init_rvalue,0;
    push @init_value ,"N/A";

} 

 
open(IN,"cat " .join(" ", @unmapped) ."|");
my %count_unmapped;
my %unmapped;
while(<IN>){
    chomp;
    my @data = (split(/\_/,$_));
    my $prefix = shift @data;
    my $gene_id = join("_",@data);
    $count_unmapped{$prefix}++;
    $unmapped{$prefix}{$gene_id} = 1;
}


open(IN,"cat ". join(" ",@bed) ." |");
my %gene;
my %count_gene;
my $cpt = 0;
while(<IN>){
    chomp;
    my ($chr, $start,$end, $id) = (split(/\t/,$_));
    my $size = int($end-$start);
    my @data = (split(/\_/,$id));
    my $prefix = shift @data;
    my $gene_id = join("_",@data);
    $count_gene{$prefix}++;
    if ($unmapped{$prefix}{$gene_id} ) {
        next;
    }
    next if ($unmapped{$prefix}{$gene_id});
    $gene{$prefix}{$gene_id} = $size;    
}
close IN; 
open(TEMP,">$temp_pav");
foreach my $file (@comparison){ 
    my ($name,$path) = fileparse($file,".bed");
    my ($first,$second) = (split(/\_/,$name)); 
    my $cpt = 0;
    open(IN,$file);
    while(<IN>){
        chomp;
        my ($gene1,$gene2) = (split(/\t/,$_)); 
        $graph->add_edge($gene1,$gene2);
        $cpt++;   
    }
}
my @cc = $graph->connected_components();

foreach my $component (@cc){
    print TEMP join("\t", @$component),"\n";
}
close TEMP;

open(IN,$temp_pav);
open(OUT,">$pav");
open(R,">$upset");

print R join("\t","Id",@species),"\n";
my $cpt = 0;
my %pav;
while(<IN>){
    chomp;
    my @data = (split(/\t/,$_));
    $cpt++;
    for (my $i=0;$i<=$#data;$i++){
        my @gene_list = (split(/\_/,$data[$i]));
        my $prefix = shift @gene_list;
        my $gene_id = join("_",@gene_list); 
        push @{$pav{$cpt}{$prefix}}, $gene_id;
    }
}
my %size;
foreach my $id (keys%pav){
    $count++;
    my @result;
    my @result_r;
    for (my $i = 0; $i<=$#species; $i++){ 
        my $species = $species[$i]; 
        if ($pav{$id}{$species}) { 
            push @result ,join(",",@{$pav{$id}{$species}});
            foreach my $is_in_pav (@{$pav{$id}{$species}}) {
                $cpt_delete{$species}++;
                if ($size{$count}){
                    if ($gene{$species}{$is_in_pav} > $size{$count}->{size}){
                        $size{$count} = {
                            name => $is_in_pav,
                            size => $gene{$species}{$is_in_pav}
                        }; 
                    }
                }
                else {
                    $size{$count} = {
                        name => $is_in_pav,
                        size => $gene{$species}{$is_in_pav}
                    };
                }
                delete($gene{$species}{$is_in_pav});
            }
            if ($i == 0) {  
                push @result_r , $count ,1;
            }
            else {
                push @result_r , 1;
            }
            
        }
        else {
            push @result,"N/A";
            if ($i == 0) {  
                push @result_r , $count ,0;
            }
            else {
                push @result_r , 0;
            }
        }
    }
    print OUT join("\t",@result),"\n";
    print R join("\t",@result_r),"\n";
} 

for (my $i = 0; $i<=$#species; $i++){ 
    my $species = $species[$i]; 
    if ($gene{$species}) {
        foreach my $id (keys %{$gene{$species}}) {
            my @values = @init_value;
            my @rvalues = @init_rvalue;
            $cpt_alone{$species}++;
            $count++;
            $values[$i] = $id;
            $rvalues[$i] = 1;
            $size{$count} = {
                name => $id,
                size => $gene{$species}{$id}
            };
            delete($gene{$species}{$id});
            print OUT join("\t",@values) ,"\n"; 
            print R join("\t",$count,@rvalues),"\n";  
        }
    }
}

close OUT;
close R;
system("sort $pav > $pav_sorted ");
open(STAT,">$stat");
print STAT join("\t","Species","Num of genes","Num of gene unmapped","Num Gene present on group","Num of gene alone"),"\n";
foreach my $species (keys %count_gene) {
    if ($count_unmapped{$species} eq "") {
        $count_unmapped{$species} = 0;
    } 
    print STAT join("\t",$species,$count_gene{$species},$count_unmapped{$species},$cpt_delete{$species},$cpt_alone{$species}),"\n";
} 
close STAT;

open(KEEP,">$keep_id");
foreach my $num (sort {$a->{name} cmp $b->{name}} keys %size){
    print KEEP $size{$num}->{name} ,"\n";
}
close KEEP;

system("sort $keep_id > $keep_id_sorted");
