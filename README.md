# Snakemake workflow: Pangenome construction using cactus-pangenome

[![Snakemake](https://img.shields.io/badge/snakemake-≥6.3.0-brightgreen.svg)](https://snakemake.github.io)
[![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](http://www.gnu.org/licenses/gpl.html)       

**Table of Contents**
 
  - [Objective](#objective)
  - [Dependencies](#dependencies)
  - [Overview of programmed workflow](#overview-of-programmed-workflow)
  - [Procedure](#procedure)
  - [Output directory](#output-directory)
 
## Objective

Create pangenome graph using minigraph cactus

Identify Core and Accessory Genes

## Dependencies

* cactus-pangenome : Minigraph-Cactus Pangenome Pipeline (https://github.com/ComparativeGenomicsToolkit/cactus/blob/master/doc/pangenome.md#the-minigraph-cactus-pangenome-pipeline)
* gfatools : A set of tools for manipulating sequence graphs in the GFA format (https://github.com/lh3/gfatools)
* bedtools intersect : Allows one to screen for overlaps between two sets of genomic features (https://bedtools.readthedocs.io/en/latest/content/tools/intersect.html)
* perl-graph : Graph data structures and algorithms (https://metacpan.org/dist/Graph/view/lib/Graph.pod)
* tabix : Generic indexer for TAB-delimited genome position files (http://www.htslib.org/doc/tabix.html)
* liftoff : Liftoff is a tool that accurately maps annotations in GFF or GTF between assemblies of the same, or closely-related species. (https://github.com/agshumate/Liftoff)
* upsetR : Draw upset plot (https://www.rdocumentation.org/packages/UpSetR/versions/1.4.0)

## Overview of programmed workflow

<p align="center">
<img src="images/rulegraph.svg" width="50%">
</p>


## Procedure 

- Clone the code repository from github to your computer by running the
  following shell command in your terminal:
  ```
  git clone https://gitlab.cirad.fr/umr_agap/workflows/cactus-pangenome.git
  ```


- Change directories into the cloned repository:
  ```
  cd cactus-pangenome
  ```

- Edit the configuration file config.yaml

```
# input_genome = For each organism define by the first keys (i.e ENSGL), you need to provide several file or value
# - fasta = Assembly as fasta
# - gff3 = Annotation file in GFF format, for each mRNA, we used the tag value Name which should be same as fasta file 
# - name = Species name

input_genome:
  "CITRE":
    "fasta": "/storage/replicated/cirad/web/HUBs/citrus/citrus_reticulata/CITRE.fasta"
    "gff3": "/storage/replicated/cirad/web/HUBs/citrus/citrus_reticulata/CITRE.gff3"
    "name": "C. reticulata"
  "CITME":
    "fasta": "/storage/replicated/cirad/web/HUBs/citrus/citrus_medica/CITME.fasta"
    "gff3": "/storage/replicated/cirad/web/HUBs/citrus/citrus_medica/CITME.gff3"
    "name": "C. medica" 
  "CITMI":
    "fasta": "/storage/replicated/cirad/web/HUBs/citrus/citrus_micrantha/CITMI.fasta"
    "gff3": "/storage/replicated/cirad/web/HUBs/citrus/citrus_micrantha/CITMI.gff3" 
    "name": "C. micrantha" 
  "CITMA":
    "fasta": "/storage/replicated/cirad/web/HUBs/citrus/citrus_maxima/CITMA.fasta"
    "gff3": "/storage/replicated/cirad/web/HUBs/citrus/citrus_maxima/CITMA.gff3"
    "name": "C. maxima" 
 
# Pivot for cactus
reference: "CITRE" 

threads: 12

cactus:
  advanced_parameter: " --gfa clip full --gbz --viz "

# remove sequences that are shorter than a specified length
seq_length: 200

# The minimum required overlap between set A and set B, expressed as a fraction
bedtools:
  coverage: 0.2

```

- Check the config profile (profile/config.yaml)


- Print shell commands to validate your modification

```
module load snakemake/7.15.1-conda
module load singularity/3.5
snakemake --profile profile  --jobs 200 --printshellcmds --dryrun --use-singularity --cores 1
```

- Run workflow on Meso@LR

```
sbatch snakemake.sh
```

## Output : 

* PAV matrix : results/pav.txt

| CITRE |CITME |CITMI |CITMA |
|--|--|--|--|
| CITRE_001g000010 | CITME_001g000010 | CITMI_001g000010 | CITMA_004g000020 |
 | CITRE_001g000020,CITRE_001g000030 |  CITME_001g000020 | 	CITMI_001g000020 | 	N/A | 
 | CITRE_001g000040 | CITME_001g000030 | CITMI_001g000030 | 	N/A | 
 | CITRE_001g000050 | CITME_001g000040 | CITMI_001g000040 | 	CITMA_004g000030 | 
 | CITRE_001g000060 | CITME_001g000050 | CITMI_001g000050 | 	CITMA_004g000040 | 
 | CITRE_001g000070 | CITME_001g000060 | CITMI_001g000060 | 	CITMA_004g000050 | 
 | CITRE_001g000080 | CITME_001g000080 | CITMI_001g000070 | 	CITMA_004g000060 | 
 | CITRE_001g000090 | CITME_001g000090 | CITMI_001g000080 | 	CITMA_004g000070 | 
 | CITRE_001g000100 | CITME_001g000100 | CITMI_001g000090 | 	CITMA_004g000080 | 
 | CITRE_001g000110 | N/A | CITMI_001g000110,CITMI_001g000120 | 	N/A | 

* Upset plot : results/pav.png
<p align="center">
<img src="images/pav.png" width="50%">
</p>
