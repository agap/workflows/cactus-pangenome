import itertools
import snakemake
import random
import os
import sys
from Bio import SeqIO
import yaml
configfile: "config.yaml"

from itertools import combinations

# Globals ---------------------------------------------------------------------
input_genome=config["input_genome"]
list_genome=input_genome.keys() 
list_combinations = ['_'.join(pair) for pair in itertools.combinations(list_genome, 2)]
 
wildcard_constraints:
    genome= "|".join(input_genome.keys()),

def generate_color():
    color = '#{:02x}{:02x}{:02x}'.format(*map(lambda x: random.randint(0, 255), range(3)))
    return color

def get_mem_liftoff(wildcards, threads):
    return threads * 4096

rule all:
    input:
        "results/pangenome.fa",
        expand("results/{genome}.bed",genome=input_genome.keys()),
        expand("results/{combination}.bed",combination=list_combinations),
        "results/pav.txt",
        "results/pav.png"

# Write input file for cactus-pangenome
rule get_data:
    input:
        "config.yaml"   
    output:
        temp("logs/seqfile")
    run:
        with open(input[0], "r") as yaml_file, open(output[0], "w") as output_file:
            genome_data = yaml.safe_load(yaml_file)
            for genome_name, genome_info in genome_data["input_genome"].items():
                fasta_file = genome_info["fasta"]
                output_file.write(f"{genome_name}\t{fasta_file}\n")

# run cactus 
rule cactus:
    input:
        rules.get_data.output  
    output:
        gfa = "results/pangenome.sv.gfa.gz"
    params:
        outdir = "results",
        workdir  = "logs/work_cactus",
        jobstore  = "logs/jobstore",
        reference = config["reference"],
        threads = config["threads"],
        advanced_parameter = config["advanced_parameter"]
    threads:
        config["threads"]
    singularity:
        config["containers"]["cactus"]
    shell:"""
        rm -Rf {params.jobstore};
        mkdir -p {params.workdir} {params.outdir};
        cactus-pangenome {params.jobstore} {input} --outDir {params.outdir} --outName pangenome --reference {params.reference} --workDir {params.workdir} --mapCores {params.threads} --gfa clip full --gbz --viz
    """

# Convert gfa to fasta
rule gfatools:
    input:
        rules.cactus.output.gfa
    output:
        temp("results/pangenome_temp.fa")
    params:
        reference = config["reference"]    
    singularity:
        config["containers"]["cactus"]
    shell:"""
        gfatools gfa2fa -s {input} > {output}
    """
 
# Remove short sequence and reformat header
rule fasta_header:
    input:
        fasta = rules.gfatools.output
    output:
        fasta = "results/pangenome.fa"
    params:
        seq_length = config["seq_length"]
    run:
        with open(input[0]) as original, open(output[0], 'w') as modified:
            records = SeqIO.parse(original, 'fasta')
            for record in records:
                if len(record.seq) >= params.seq_length:
                    record.description = ""
                    record.id = record.id.strip().split('|')[1] 
                    SeqIO.write(record, modified, 'fasta') 


rule liftoff:
    input:
        fasta = rules.fasta_header.output,
        gff3  = lambda wildcards: input_genome[wildcards.genome]["gff3"],
        assembly = lambda wildcards: input_genome[wildcards.genome]["fasta"] 
    output:
        gff3 = "results/{genome}.gff3",
        unmapped = "results/{genome}_unmapped.txt" 
    params:
        directory = "logs/{genome}",
        threads = config["threads"]
    threads:
        config["threads"]
    singularity:
        config["containers"]["liftoff"]
    shell:"""
        liftoff -p {params.threads} -dir {params.directory} -g {input.gff3} -o {output.gff3} -u {output.unmapped} {input.fasta} {input.assembly}
    """


rule add_species:
    input:
        "results/{genome}_unmapped.txt"
    output:
        "results/{genome}_unmapped_with_species.txt"
    params:
        genome = "{genome}"
    shell:"""
        sed -e 's/^/{params.genome}_/' {input} > {output}
    """

rule gff2bed:
    input:
        rules.liftoff.output.gff3
    output:
        "results/{genome}.bed"
    params:
        genome = "{genome}"
    shell:"""
        scripts/gff2bed.pl {input} {params.genome} {output}
    """



rule bedtools:
    input:
        bed1 = "results/{first}.bed",
        bed2 = "results/{second}.bed"
    output:
        "results/{first}_{second}.bed"
    params:
        coverage = config["bedtools"]["coverage"]
    singularity:
        config["containers"]["bedtools"]
    shell:"""
        bedtools intersect -a {input.bed1} -b {input.bed2}  -f {params.coverage} -F {params.coverage}  -wao | cut -f 4,8 | grep -v '\\.' > {output}     
    """

rule graph:
    input:
        bed = expand("results/{genome}.bed",genome=input_genome.keys()),
        comparison = expand("results/{combination}.bed",combination=list_combinations),
        unmapped = expand("results/{genome}_unmapped_with_species.txt",genome=input_genome.keys())
    output:
        pav = "results/pav.txt",
        upset = "results/pav.csv",
        stat = "results/pav.stat",
        list_id = "results/keep.txt"
    params:
        directory = "results"
    singularity:
        config["containers"]["perl-graph"]
    shell:"""
        perl scripts/graph.pl --comparison {input.comparison} --bed {input.bed} --unmapped {input.unmapped} --directory {params.directory}
    """

rule upset_plot:
    input:
        pav = rules.graph.output.upset
    output:
        png = "results/pav.png" 
    singularity:
        config["containers"]["complexheatmap"]
    script:
        "./scripts/upset_with_complexheatmap.R"